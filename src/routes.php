<?php

use Slim\Http\Request;
use Slim\Http\Response;
use \Firebase\JWT\JWT;
use Tuupola\Middleware\CorsMiddleware;

// Routes

/* $app->get('/[{name}]', function (Request $request, Response $response, array $args) { */
/*     // Sample log message */
/*     $this->logger->info("Slim-Skeleton '/' route"); */

/*     // Render index view */
/*     return $this->renderer->render($response, 'index.phtml', $args); */
/* }); */



$app->add(new \Tuupola\Middleware\CorsMiddleware([
	"origin" => ["*"],
	"methods" => ["GET", "POST", "PUT", "PATCH", "DELETE"],
	"headers.allow" => ["Authorization", "If-Match", "If-Unmodified-Since", "Accept",  "Content-Type"],
	"headers.expose" => ["Etag"],
	"credentials" => true,
	"credentials" => false,
	"cache" => 86400,
	"logger" => $container['logger']
]));


$app->get('/hi', function (Request $request, Response $response, array $args) {
	// Sample log message
	$this->logger->info("Slim-Skeleton '/' route");

	// Render index view
	return $this->renderer->render($response, 'index.phtml', $args);
});

$app->post('/login', function (Request $request, Response $response, array $args) {

	$input = $request->getParsedBody();
	$sql = "SELECT * FROM users WHERE email= :email";
	$sth = $this->db->prepare($sql);
	$sth->bindParam("email", $input['email']);
	$sth->execute();
	$user = $sth->fetchObject();

	// verify email address.
	if(!$user) {
		return $this->response->withStatus(401)->withJson(['error' => true, 'message' => 'These credentials do not match our records.']);
	}

	// verify password.
	if (!password_verify($input['password'],$user->password)) {
		return $this->response->withStatus(401)->withJson(['error' => true, 'message' => 'These credentials do not match our records.']);
	}

	$settings = $this->get('settings'); // get settings array.

	$startDate = time();
	$newDate = strtotime('+1 month',$startDate);


	$token = JWT::encode(['id' => $user->id, 'email' => $user->email, 'exp' => $newDate], $settings['jwt']['secret'], "HS256");

	unset( $user->password );
	return $this->response->withJson(['user' => $user, 'token' => $token]);

});

$app->post('/register', function (Request $request, Response $response, array $args) {

	$input = $request->getParsedBody();

	$email = $input["email"];
	$name = $input["name"];
	$password = $input["password"];

	if ( empty($email) ) {
		return $this->response->withStatus(400)->withJson(['error' => true, 'message' => 'email is empty']);
	}

	if ( empty($name) ) {
		return $this->response->withStatus(400)->withJson(['error' => true, 'message' => 'username is empty']);
	}

	if ( empty($password) ) {
		return $this->response->withStatus(400)->withJson(['error' => true, 'message' => 'password is empty']);
	}

	$password_hash = password_hash($password, PASSWORD_BCRYPT);

	$sql = "INSERT INTO users ( name, email, password ) VALUES (:name, :email, :password)";
	$sth = $this->db->prepare($sql);

	$sth->bindParam("email", $email);
	$sth->bindParam("name", $name);
	$sth->bindParam("password", $password_hash);
	$sth->execute();

	return $this->response->withJson(['message' => 'success' ]);

});

// Private routes

$app->group('/api', function(\Slim\App $app) {

	$app->get('/user',function(Request $request, Response $response, array $args) {
		print_r($request->getAttribute('decoded_token_data'));

	/*output
	stdClass Object
	    (
		[id] => 2
		[email] => arjunphp@gmail.com
	    )

	 */
	});

	$app->post('/add',function(Request $request, Response $response, array $args) {
		$user = $request->getAttribute('decoded_token_data');
		$input = $request->getParsedBody();

		$name = $input["habit_name"];
		$question = $input["question"];

		if ( empty($name) ) {
			return $this->response->withStatus(400)->withJson(['error' => true, 'message' => 'email is empty']);
		}

		$sql = "INSERT INTO habits ( user_id , name, question ) VALUES (:userid, :hname, :hquestion)";
		$sth = $this->db->prepare($sql);

		$sth->bindParam("userid", $user["id"]);
		$sth->bindParam("hname", $name);
		$sth->bindParam("hquestion", $question);
		$sth->execute();

		return $this->response->withJson(['message' => 'success' ]);

	});

	$app->get('/habits',function(Request $request, Response $response, array $args) {
		$user = $request->getAttribute('decoded_token_data');
		$input = $request->getParsedBody();

		$sql = "SELECT * FROM habits WHERE user_id = :uid";
		$sth = $this->db->prepare($sql);

		$sth->bindParam("uid", $user["id"]);
		$sth->execute();

		/* while ($row = mysql_fetch_array($sth, MYSQL_ASSOC)) { */
		/* array_push($return_arr,$row_array); */
		/* } */


		return $this->response->withJson($sth->fetchAll());
	});

	$app->get('/today',function(Request $request, Response $response, array $args) {
		$user = $request->getAttribute('decoded_token_data');
		$input = $request->getParsedBody();

		$sql = " SELECT * FROM habits WHERE
	user_id = :uid
	AND habits.id IN
	(
		SELECT habit_id FROM diary
		WHERE answered = FALSE
		AND today = CURRENT_DATE()
	)
";
		$sth = $this->db->prepare($sql);
		$sth->bindParam("uid", $user["id"]);
		$sth->execute();
		/* while ($row = mysql_fetch_array($sth, MYSQL_ASSOC)) { */
		/* array_push($return_arr,$row_array); */
		/* } */

		return $this->response->withJson($sth->fetchAll());
	});

	$app->get('/today/{id}/yes',function(Request $request, Response $response, array $args) {
		$user = $request->getAttribute('decoded_token_data');
		$habit_id = $args["id"];


		$sql = " UPDATE diary SET answered = TRUE, checked = NOT checked
			WHERE habit_id = :hid AND today = CURRENT_DATE()
			AND EXISTS (
				SELECT habits.id, habits.user_id
				FROM habits WHERE habits.id = :hid
				AND habits.user_id = :uid )";

		$sth = $this->db->prepare($sql);
		$sth->bindParam("uid", $user["id"]);
		$sth->bindParam("hid", $habit_id);
		$sth->execute();
		/* while ($row = mysql_fetch_array($sth, MYSQL_ASSOC)) { */
		/* array_push($return_arr,$row_array); */
		/* } */

		return $this->response->withJson(['message' => 'success']);
	});

	$app->get('/today/{id}/no',function(Request $request, Response $response, array $args) {
		$user = $request->getAttribute('decoded_token_data');
		$habit_id = $args["id"];


		$sql = " UPDATE diary SET answered = TRUE, checked = NOT checked
			WHERE habit_id = :hid AND today = CURRENT_DATE()
			AND EXISTS (
				SELECT habits.id, habits.user_id
				FROM habits WHERE habits.id = :hid
				AND habits.user_id = :uid )";

		$sth = $this->db->prepare($sql);
		$sth->bindParam("uid", $user["id"]);
		$sth->bindParam("hid", $habit_id);
		$sth->execute();
		/* while ($row = mysql_fetch_array($sth, MYSQL_ASSOC)) { */
		/* array_push($return_arr,$row_array); */
		/* } */

		return $this->response->withJson(['message' => 'success']);
	});

	$app->get('/data/habit/{id}',function(Request $request, Response $response, array $args) {
		$user = $request->getAttribute('decoded_token_data');
		$input = $request->getParsedBody();

		$sql = " SELECT today, checked FROM diary WHERE habit_id = :hid";
		$sth = $this->db->prepare($sql);
		$sth->bindParam("hid", $args["id"]);
		$sth->execute();
		/* while ($row = mysql_fetch_array($sth, MYSQL_ASSOC)) { */
		/* array_push($return_arr,$row_array); */
		/* } */

		return $this->response->withJson($sth->fetchAll());
	});

	$app->get('/data/habit/strength/{id}',function(Request $request, Response $response, array $args) {
		$user = $request->getAttribute('decoded_token_data');
		$input = $request->getParsedBody();

		$sql = " SELECT today, checked FROM diary WHERE habit_id = :hid";
		$sth = $this->db->prepare($sql);
		$sth->bindParam("hid", $args["id"]);
		$sth->execute();
		/* while ($row = mysql_fetch_array($sth, MYSQL_ASSOC)) { */
		/* array_push($return_arr,$row_array); */
		/* } */
		$totarray = sizeof($sth->fetchAll);

		$sql = " SELECT today, checked FROM diary WHERE habit_id = :hid AND checked = 1";
		$sth = $this->db->prepare($sql);
		$sth->bindParam("hid", $args["id"]);
		$sth->execute();
		$checkarray = sizeof($sth->fetchAll);

		$reply = json_encode(unserialize(str_replace(array('NAN;','INF;'),'0;',serialize($checkarray/$totarray))));

		return $this->response->withJson(['strenght' => $reply, 'message' => 'success']);
	});


});
